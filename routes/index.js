var router = require('koa-router')();

router.get('/', function *(next) {
  yield this.render('index', {
    title: 'Hello World Koa!'
  });
});

router.get('/foo', function *(next) {
  yield this.render('index', {
    title: 'Hello World foo!'
  });
});

router.get('/test', function *(next) {
    console.log('this.request.query',this.request.query);
    this.body = this.request.query.username;
});

router.post('/posttest', function *(next) {
  console.log('响应 post 请求！');
  const params = this.request.body;
  console.log('params',params);
  this.body = 'success';
});

module.exports = router;
